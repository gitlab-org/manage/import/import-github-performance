FROM registry.gitlab.com/gitlab-org/gitlab-environment-toolkit:3.5.2

ENV BUNDLE_WITHOUT=development

WORKDIR /gitlab-environment-toolkit

# Install system tools
#
RUN set -eux; \
  apt-get update && apt-get install --no-install-recommends -y \
  build-essential \
  libz-dev \
  libssl-dev; \
  apt-get autoremove -yq \
  && apt-get clean -yqq \
  && rm -rf /var/lib/apt/lists/*;

# Install ruby and terraform and ansible dependencies
COPY .tool-versions ./
RUN set -eux; \
  mise install; \
  ansible-galaxy install googlecloudplatform.google_cloud_ops_agents

# Install gems
#
COPY Gemfile Gemfile.lock ./
RUN bundle install
COPY script/prevent_env_stop.rb ./

# Copy custom tasks and monitoring setup
#
COPY deploy/monitor/dashboards ansible/environments/monitor/
COPY deploy/monitor/datasources ansible/environments/monitor/
COPY deploy/monitor/ops_agent.yaml.j2 ansible/environments/monitor/

# Set up terraform and ansible
#
ARG TF_HTTP_USERNAME
ARG TF_HTTP_PASSWORD

# Setup source 1k environment
COPY deploy/configs/source-1k/terraform terraform/environments/source-1k
RUN cd terraform/environments/source-1k && terraform init
COPY deploy/configs/source-1k/ansible ansible/environments/source-1k
COPY deploy/tasks ansible/environments/source-1k/files/gitlab_tasks/
COPY deploy/custom_configs ansible/environments/source-1k/files/gitlab_configs/

# Setup 1k environment
COPY deploy/configs/1k/terraform terraform/environments/1k
RUN cd terraform/environments/1k && terraform init
COPY deploy/configs/1k/ansible ansible/environments/1k
COPY deploy/tasks ansible/environments/1k/files/gitlab_tasks/
COPY deploy/custom_configs ansible/environments/1k/files/gitlab_configs/

# Setup 2k environment
COPY deploy/configs/2k/terraform terraform/environments/2k
RUN cd terraform/environments/2k && terraform init
COPY deploy/configs/2k/ansible ansible/environments/2k
COPY deploy/tasks ansible/environments/2k/files/gitlab_tasks/
COPY deploy/custom_configs ansible/environments/2k/files/gitlab_configs/

# Setup 3k environment
COPY deploy/configs/3k/terraform terraform/environments/3k
RUN cd terraform/environments/3k && terraform init
COPY deploy/configs/3k/ansible ansible/environments/3k
COPY deploy/tasks ansible/environments/3k/files/gitlab_tasks/
COPY deploy/custom_configs ansible/environments/3k/files/gitlab_configs/
