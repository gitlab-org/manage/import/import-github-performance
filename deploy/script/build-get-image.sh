#!/bin/bash

source "$(dirname "$0")/utils.sh"

set -e

image="${CI_REGISTRY_IMAGE}"
sha_tag="${CI_COMMIT_SHA}"
branch_tag="${CI_COMMIT_REF_SLUG}"

log_with_header "Building GET docker image: '${image}:${sha_tag}'"
docker buildx create --use
docker buildx build \
  --cache-to="type=inline" \
  --cache-from="${image}:${branch_tag}" \
  --cache-from="${image}:main" \
  --build-arg "TF_HTTP_USERNAME=$TF_HTTP_USERNAME" \
  --build-arg "TF_HTTP_PASSWORD=$TF_HTTP_PASSWORD" \
  --tag="${image}:${sha_tag}" \
  --tag="${image}:${branch_tag}" \
  --provenance=false \
  --push \
  .
