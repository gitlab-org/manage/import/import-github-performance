terraform {
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/28409791/terraform/state/source-1k"
  }
  required_providers {
    google = {
      source  = "hashicorp/google"
    }
  }
}

provider "google" {
  project = var.project
  region  = var.region
  zone    = var.zone
}
