module "gitlab_ref_arch_gcp" {
  source = "../../modules/gitlab_ref_arch_gcp"

  prefix                 = "${var.prefix}-2k"
  project                = var.project
  service_account_prefix = "gl2k"

  gitaly_node_count   = 1
  gitaly_machine_type = "n1-standard-4"

  gitlab_rails_node_count   = 2
  gitlab_rails_machine_type = "custom-8-10752" # custom type with increased memory compared to "n1-highcpu-8"

  haproxy_external_node_count   = 1
  haproxy_external_machine_type = "n1-highcpu-2"
  haproxy_external_external_ips = [var.external_ip_2k]

  opensearch_vm_node_count   = 1
  opensearch_vm_machine_type = "n1-highcpu-8"
  opensearch_vm_disk_type    = "pd-balanced"
  opensearch_vm_disk_size    = "100"

  postgres_node_count   = 1
  postgres_machine_type = "n1-standard-2"
  postgres_disk_size    = "150"

  redis_node_count   = 1
  redis_machine_type = "n1-standard-2"

  sidekiq_node_count   = 2
  sidekiq_machine_type = "n1-standard-4"

  monitor_node_count   = 1
  monitor_machine_type = "n1-highcpu-2"

  machine_image = var.machine_image
}

# Add roles to rails and redis service account for ops agent
resource "google_project_iam_member" "rails-roles" {
  for_each = toset(["roles/logging.logWriter", "roles/monitoring.metricWriter"])
  project  = var.project
  member   = "serviceAccount:${module.gitlab_ref_arch_gcp.gitlab_rails.service_account.email}"
  role     = each.key
}

resource "google_project_iam_member" "redis-roles" {
  for_each = toset(["roles/logging.logWriter", "roles/monitoring.metricWriter"])
  project  = var.project
  member   = "serviceAccount:${module.gitlab_ref_arch_gcp.redis.service_account.email}"
  role     = each.key
}
