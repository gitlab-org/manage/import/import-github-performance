module "gitlab_ref_arch_gcp" {
  source = "../../modules/gitlab_ref_arch_gcp"

  prefix                 = "${var.prefix}-3k"
  project                = var.project
  service_account_prefix = "gl3k"

  # 3k
  consul_node_count   = 3
  consul_machine_type = "n1-highcpu-2"

  gitaly_node_count   = 3
  gitaly_machine_type = "n1-standard-4"

  praefect_node_count   = 3
  praefect_machine_type = "n1-highcpu-2"

  praefect_postgres_node_count   = 1
  praefect_postgres_machine_type = "n1-highcpu-2"

  gitlab_rails_node_count   = 3
  gitlab_rails_machine_type = "n1-highcpu-8"

  haproxy_external_node_count   = 1
  haproxy_external_machine_type = "n1-highcpu-2"
  haproxy_external_external_ips = [var.external_ip_3k]
  haproxy_internal_node_count   = 1
  haproxy_internal_machine_type = "n1-highcpu-2"

  monitor_node_count   = 1
  monitor_machine_type = "n1-highcpu-2"

  opensearch_vm_node_count   = 2
  opensearch_vm_machine_type = "n1-highcpu-8"
  opensearch_vm_disk_type    = "pd-balanced"
  opensearch_vm_disk_size    = "100"

  pgbouncer_node_count   = 3
  pgbouncer_machine_type = "n1-highcpu-2"

  postgres_node_count   = 3
  postgres_machine_type = "n1-standard-2"
  postgres_disk_size    = "300"

  redis_node_count   = 3
  redis_machine_type = "n1-standard-2"

  sidekiq_node_count   = 4
  sidekiq_machine_type = "n1-standard-2"
}

# Add roles to rails and sidekiq service account for ops agent
locals {
  ops_agent_roles = toset(["roles/logging.logWriter", "roles/monitoring.metricWriter"])
}

resource "google_project_iam_member" "rails-roles" {
  for_each = local.ops_agent_roles
  project  = var.project
  member   = "serviceAccount:${module.gitlab_ref_arch_gcp.gitlab_rails.service_account.email}"
  role     = each.key
}

resource "google_project_iam_member" "sidekiq-roles" {
  for_each = local.ops_agent_roles
  project  = var.project
  member   = "serviceAccount:${module.gitlab_ref_arch_gcp.sidekiq.service_account.email}"
  role     = each.key
}
