variable "external_ip_3k" {
}

variable "project" {
  default = "dev-import-metrics-1fd5a570"
}

variable "region" {
  default = "us-central1"
}

variable "zone" {
  default = "us-central1-c"
}

variable "prefix" {
  default = "import-metrics"
}
