#!/usr/bin/env ruby

# frozen_string_literal: true

require 'terminal-table'
require 'slack-notifier'
require 'json'

# rubocop:disable Metrics/ClassLength, Metrics/AbcSize, Metrics/MethodLength

# Import result reporter
#
class ResultReporter
  IMPORTER_EMOJI = {
    'github' => ':github_octocat:',
    'gitlab' => ':ukraine-tanuki:'
  }.freeze

  def initialize(data_file = nil)
    @import_status = ENV.fetch('IMPORT_STATUS', nil)
    @pipeline_url = ENV.fetch('CI_PIPELINE_URL', nil)
    @webhook_url = ENV.fetch('CI_SLACK_WEBHOOK_URL', nil)
    @channel = ENV.fetch('CI_SLACK_CHANNEL', nil)
    @data_file = data_file

    raise 'CI_SLACK_WEBHOOK_URL and CI_SLACK_CHANNEL environment variables must be set' unless @webhook_url && @channel
  end

  # Run notification
  #
  # @return [void]
  def notify
    return notify_import_status if data_file.nil?
    return notify_import_not_finished unless import_finished?

    notify_import_finished
  rescue StandardError => e
    puts '[ERROR] Failed posting to Slack'
    puts "[ERROR] #{e.message}"
    e.backtrace&.each { |it| puts "[ERROR] #{it}" }
    exit(1)
  end

  private

  attr_reader :pipeline_url,
              :webhook_url,
              :channel,
              :data_file

  # Notify import status
  #
  # @return [void]
  def notify_import_status
    puts "notify_import_status"
    msg = []
    msg << "Import completed but there is no data file, please see the pipeline below\n"
    msg << 'Status: :exclamation:'
    msg << "Pipeline URL: #{pipeline_url}"

    notifier.post(text: msg.join("\n"), icon_emoji: ':gitlab-bot:', username: 'gitlab-bot')
    puts "[OK] Reported to Slack, #{channel} channel"
  end 

  # Notify import finished and report imported object cound
  #
  # @return [void]
  def notify_import_finished
    puts "notify_import_finished"
    msg = []
    msg << "Project `#{source_project_name}` imported from `#{source_env}` in *#{import_time}*!\n"
    msg << "Status: #{import_status == 'passed' ? ':white_check_mark:' : ':x:'}"
    msg << "Environment Type: `#{env_type}`"
    msg << "Pipeline URL: #{pipeline_url}"
    msg << "Target URL: #{target_env}/#{target_project_name}"
    msg << "Source URL: #{source_env}/#{source_project_name}"
    msg << "```\n#{importer_stats_table}\n```" if importer_stats_table
    msg << "```\n#{result_table}\n```" if result_table

    notifier.post(text: msg.join("\n"), **slack_args)
    puts "[OK] Reported to Slack, #{channel} channel"
  end

  # Notify import did not finish at all
  #
  # @return [void]
  def notify_import_not_finished
    puts "notify_import_not_finished"
    msg = []
    msg << "Project `#{source_project_name}` import from `#{source_env}` failed to complete in *#{import_time}*!\n"
    msg << 'Status: :x:'
    msg << "Environment Type: `#{env_type}`"
    msg << "Pipeline URL: #{pipeline_url}"
    msg << "Target URL: #{target_env}/#{target_project_name}"
    msg << "Source URL: #{source_env}/#{source_project_name}"

    notifier.post(text: msg.join("\n"), **slack_args)
    puts "[OK] Reported to Slack, #{channel} channel"
  end

  # Stats reported by importer itself
  # Not applicable for all importers
  #
  # @return [Terminal::Table]
  def importer_stats_table
    return unless stats

    @importer_stats_table ||= comparison_table(
      source_data: stats[:fetched],
      target_data: stats[:imported],
      title: 'Importer stats',
      headings: %w[Fetched Imported]
    )
  end

  # Result table
  #
  # @return [Terminal::Table]
  def result_table
    return @result_table if defined?(@result_table)

    source_result = data_json[:source]
    target_result = data_json[:target]
    source_data = source_result[:data]
    target_data = target_result[:data]
    return @result_table = nil unless source_data && target_data

    @result_table ||= comparison_table(
      source_data: source_data,
      target_data: target_data,
      title: 'Test comparison result',
      headings: [source_result[:name], target_result[:name]]
    )
  end

  # Get table with comparison between source and target data
  #
  # @param [Hash] source_data
  # @param [Hash] target_data
  # @param [Hash] title
  # @param [Array] headings
  # @return [Terminal::Table]
  def comparison_table(source_data:, target_data:, title:, headings:)
    result = source_data.to_h do |k, v|
      next [k, '✅'] if target_data[k] == v
      next [k, '❌'] if target_data[k] < v
      next [k, '❗'] if target_data[k] > v
    end
    Terminal::Table.new(headings: ['', *headings, 'Result'], style: table_style, title: title) do |table|
      source_data.each_with_index do |(k, v), index|
        table << [k, v, target_data[k], result[k]]
        table << :separator if index < (source_data.size - 1)
      end
    end
  end

  # Test status, passed or failed
  #
  # @return [String]
  def import_status
    data_json[:status]
  end

  # Check if import finished
  #
  # @return [Boolean] <description>
  def import_finished?
    data_json[:import_finished]
  end

  # Common notification args
  #
  # @return [Hash]
  def slack_args
    @slack_args ||= { icon_emoji: IMPORTER_EMOJI[importer_name], username: importer_name }
  end

  # Source environment
  #
  # @return [String]
  def source_env
    @source_env ||= data_json.dig(:source, :address)
  end

  # Target environment
  #
  # @return [String]
  def target_env
    @target_env ||= data_json.dig(:target, :address)
  end

  # Source project name
  #
  # @return [String]
  def source_project_name
    @source_project_name ||= data_json.dig(:source, :project_name)
  end

  # Imported project name
  #
  # @return [String]
  def target_project_name
    @target_project_name ||= data_json.dig(:target, :project_name)
  end

  # Time import took to finish
  #
  # @return [String]
  def import_time
    @import_time ||= begin
      hours = (data_json[:import_time] / 3600).round(2)

      hours >= 24 ? "#{(hours / 24).round(1)} days" : "#{hours} hours"
    end
  end

  # Environment type
  #
  # @return [String]
  def env_type
    arch = ENV.fetch('ENV_REFERENCE_ARCHITECTURE', '')
    return arch unless arch.empty?

    'external'
  end

  # Importer name
  #
  # @return [String]
  def importer_name
    @importer_name ||= data_json[:importer]
  end

  # Parsed data json
  #
  # @return [Hash]
  def data_json
    @data_json ||= JSON.parse(data, symbolize_names: true)
  end

  # Stats reported by importer
  #
  # @return [Hash]
  def stats
    @stats ||= data_json[:reported_stats]
  end

  # Data json file
  #
  # @return [String]
  def data
    @data ||= File.read(data_file)
  end

  # Slack notifier
  #
  # @return [Slack::Notifier]
  def notifier
    @notifier ||= Slack::Notifier.new(webhook_url, channel: "##{channel}")
  end

  # Table styling
  #
  # @return [Hash]
  def table_style
    @table_style ||= {
      border_left: false,
      border_right: false,
      border_top: false,
      border_bottom: false,
      width: 80
    }
  end
end

files = Dir.glob(ENV['DATA_JSON_GLOB'] || '')
if files.empty?
  msg = "Processing test results"
  puts '=' * msg.length
  puts msg
  puts '=' * msg.length
  ResultReporter.new().notify
else
  files.each do |file|
    msg = "Processing test report #{file}"
    puts '=' * msg.length
    puts msg
    puts '=' * msg.length
    ResultReporter.new(file).notify
  end
end


# rubocop:enable Metrics/ClassLength, Metrics/AbcSize, Metrics/MethodLength
