#!/usr/bin/env ruby

# frozen_string_literal: true

require "gitlab"

class PreventEnvStop
  # Wait for all test pipelines to finish before stopping environment
  #
  # @return [void]
  def wait_for_tests_to_finish # rubocop:disable Metrics/MethodLength
    errors = []

    puts "Waiting for all test runs to finish before shutting down environment"
    loop do
      raise "Max amount of errors reached while fetching running tests!" if errors.size > 5

      pipeline = pipeline_with_tests
      break unless pipeline

      puts "  running tests detected in '#{pipeline.web_url}', waiting..."
      sleep(120)
    rescue Gitlab::Error => e
      puts "  fetching ci job info failed, error: '#{e.message}'"
      errors << e.message
      sleep(1)
    end
    puts "No running tests detected, continuing environment shutdown!"
  end

  private

  # Gitlab client instance
  #
  # @return [Gitlab::Client]
  def client
    @client ||= Gitlab.client(
      endpoint: ENV.fetch("CI_API_V4_URL"),
      private_token: ENV.fetch("PROJECT_ACCESS_TOKEN")
    )
  end

  def project
    @project ||= ENV.fetch("CI_PROJECT_PATH")
  end

  # Environment stop job name
  #
  # This is used to detect pipeline of same environment type
  #
  # @return [String]
  def job_name
    @job_name ||= ENV.fetch("CI_JOB_NAME")
  end

  # Get currently running pipelines
  #
  # @return [Array<Integer>]
  def running_pipelines
    client.pipelines(project, status: "running", per_page: 100)
  end

  # Check if pipeline running tests is present
  #
  # @return [Gitlab::ObjectifiedHash]
  def pipeline_with_tests
    running_pipelines.find do |pipeline|
      jobs = client.pipeline_jobs(project, pipeline.id)
      next unless jobs.any? { |job| job.name == job_name }

      # check if import is running
      jobs.any? { |job| job.status == "running" && job.name.match?(/^(github|gitlab)-import$/) }
    end
  end
end

PreventEnvStop.new.wait_for_tests_to_finish
