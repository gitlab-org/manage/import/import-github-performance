# Importer Performance

[[_TOC_]]

Pipeline configuration to continuously validate:

* Github import functionality
* Direct transfer functionality

## Test reporting

### Grafana dashboard

Some of the results are tracked over time and can be seen via grafana dashboards:

* [Dashboards](https://dashboards.quality.gitlab.net/d/AioZ8Qd4z/large-import-tests?orgId=1)

### Slack notification

Last stage of the pipeline posts slack notification with comprehensive table of item difference to `import-export-metrics` channel.

[report.rb](script/report.rb) contains code responsible for notification creation.

`CI_SLACK_WEBHOOK_URL` environment variable in CI/CD settings defines webhook url for posting notifications

## Pipelines

### Scheduled run

[Scheduled runs](https://gitlab.com/gitlab-org/manage/import/import-metrics/-/pipeline_schedules) execute test imports of medium and large sized projects for `Direct Transfer` and `GitHub import` and report results to `import-export-metrics` slack channel.

#### Daily

`2k-medium-project-test` schedule runs daily from Monday - Wednesday and runs import of medium sized project that can be imported within 24h.

#### Weekend

`2k-large-project-test` schedule runs on Thursday through the weekend and runs import of large sized project which takes multiple days to import.

#### Inactive schedules

Because GitLab doesn't allow to preconfigure pipelines for manual execution, we use scheduled pipelines to configure manual pipeline runs.

* `env-destroy-1k` - completely destroy `1k` environment
* `env-destroy-2k` - completely destroy `2k` environment
* `env-destroy-3k` - completely destroy `3k` environment
* `env-deploy-3k` - trigger a manual deployment of `3k` environment. This will update environment to latest nightly build

### Manual runs

**:warning: Because all pipelines use the same api tokens by default, when running manual pipeline runs for GitHub import, it can affect the result of scheduled pipelines because it will affect how soon api rate limit kicks in :warning:**

To avoid impact on the automated test runs, it's possible to provide a different GitHub tokens. See [`GitHub tokens`](README.md#github-tokens) section.

Pipeline can be executed via `Run pipeline` button from [pipelines](https://gitlab.com/gitlab-org/manage/import/import-export-performance/-/pipelines) page. This is mostly useful to trigger a manual test run with specific importer, project, test environment. Following variables can be overridden:

* `QA_IMAGE` - image with the test code, default: `registry.gitlab.com/gitlab-org/gitlab/gitlab-ee-qa:master`
* `QA_LARGE_IMPORT_DURATION` - timeout in seconds test will wait for import to finish, default: `14400`
* `REPORT_SLACK` - set to `true` to post test run result to slack, default: `false`
* `ENV_REFERENCE_ARCHITECTURE` - run import on self managed environment of [reference architecture](https://docs.gitlab.com/ee/administration/reference_architectures) type, example: `2k`
* `IMPORTER_TYPE` - importer type, supported values are: `github`, `gitlab` or `github,gitlab` to run both test types in parallel
* `GITHUB_IMPORT_REPO` - full path to repository fot GitHub import test
* `DIRECT_TRANSFER_GROUP` - full path to group for direct transfer test
* `DIRECT_TRANSFER_REPO` - project name to run validation against in direct transfer test, must be in group provided via `DIRECT_TRANSFER_GROUP` variable

#### GitHub tokens

Tokens for GitHub have to be overridden manually and won't appear in the list to allow defaulting to values set in CI/CD settings

* `GITHUB_API_TOKEN_IMPORTER` - github api token used to trigger import
* `GITHUB_API_TOKEN_TEST_PROCESS` - github api token used by test process to fetch objects for comparison

## Setup

All test environment deployments are using Google Cloud Provider and are configured to use `dev-import-metrics-1fd5a570` project.

### Test environment

* Source environments: <github.com> and <gitlab.com> are used as sources instances
* Target environments: self managed install with different architecture types

#### Reference Architecture environment

CI setup uses [GET](https://gitlab.com/gitlab-org/gitlab-environment-toolkit) to deploy a `GitLab package (Omnibus)` environment to GCP. Environment status and url can be seen in [environments](https://gitlab.com/gitlab-org/manage/import/import-metrics/-/environments) page.

Currently supported architectures:

* `1k`
* `2k`
* `3k`

##### Debugging

When test run fails, test environment is shut down, but not destroyed. It can be redeployed from [environments](https://gitlab.com/gitlab-org/manage/import/import-metrics/-/environments) page and then interacted manually. Environment will retain all the data from the last run.

##### Admin credentials

Environments have admin user provisioned on them. When debugging test failure or just interacting with the environment, it possible to log in using following credentials:

* `username`: `root`
* `password`: available under CI/CD variables settings as `GITLAB_ROOT_PASSWORD` environment variable

##### Monitoring

Additionally, each test environment exposes grafana instance with dashboards for various instance metrics. Grafana instance is available at path `/-/grafana` for the respective test environment.

Credentials for grafana:

* `username`: `admin`
* `password`: available under CI/CD variables settings as `GRAFANA_PASSWORD` environment variable

##### Logs

All environment logs are available in `GCP` [Logs Explorer](https://cloud.google.com/logging/docs/view/logs-explorer-interface). To get logs of specific reference architecture environment, custom attribute `environment_type` can be used in the query, example:

```
jsonPayload.environment_type="2k"
```

Additional attribute `node_type` can be used to differentiate between logs on `sidekiq` and `rails` VM's (currently applicable only for `3k` environment, smaller environments don't have separate sidekiq VM's), example:

```
jsonPayload.node_type="sidekiq"
```

For more advanced queries, consult GCP `Logs Explorer` documentation.

### Test user credentials

Several types of API tokens are required for tests to work correctly

#### GitHub import

GitHub import test requires two api tokens in order for test process to not affect import itself:

* `GITHUB_API_TOKEN_TEST_PROCESS` - defines api token used by the test itself to fetch objects from source for comparison.
* `GITHUB_API_TOKEN_IMPORTER` - defines api token used by import process in `GitLab` rails application. Currently api token of `gitlab-qa` user is used which is is available in `Quality` vault in 1Password.

#### Direct transfer

Direct transfer require 2 api tokens:

* `QA_LARGE_IMPORT_GL_TOKEN` - defines api token in source GitLab instance. Must belong to user with owner access for group being imported. Currently api token of `gitlab-qa` user is used which is available in `Engineering` vault in 1Password.
* `GITLAB_QA_ADMIN_ACCESS_TOKEN` - defines admin api token in target GitLab instance. Test requires target instance admin token in order to create top level group for import.

### Test projects

* GitHub import uses [vuejs/vue](https://github.com/vuejs/vue) as source project for import
* GitLab import uses [Gitaly Copy](https://gitlab.com/gitlab-migration-large-import-test/gitaly-copy/gitaly-complete) as source project for import

### Test code

Code of the rspec tests themselves is located in [gitlab](https://gitlab.com/gitlab-org/gitlab) repository.

* gitlab import: <https://gitlab.com/gitlab-org/gitlab/-/blob/master/qa/qa/specs/features/api/1_manage/migration/gitlab_migration_large_project_spec.rb>
* github import: <https://gitlab.com/gitlab-org/gitlab/-/blob/master/qa/qa/specs/features/api/1_manage/import/import_large_github_repo_spec.rb>

The main structure for both tests is the same:

* Trigger import
* Fetch all objects via api from source project
* Wait for import to finish
* Fetch all objects via api from imported target project
* Compare imported objects and source objects

Each test generates `data.json` file which contains data about the project used for import and missing or extra objects.

#### Expectations

All objects in the test are compared using [rspec-expectations](https://github.com/rspec/rspec-expectations). Issues and merge requests are compared 1 by 1. In case of expectation failures error messages will follow the structure:

1. First failure error will print issue/mr iid and overview of what does not match, example: `expected issue with iid '1664' to have same amount of events. Gitlab: 3, Github: 0`
2. Next failure errors will always be relevant to issue/mr iid mentioned in the previous failure message and will contain exact diff of object types being compared, like comments or events
